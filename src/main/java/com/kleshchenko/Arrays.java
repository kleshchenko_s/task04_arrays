package com.kleshchenko;

public class Arrays {
    public static void main(String[] args) {

    }
    static int[] formNewArray(int[] arr_1, int size_1, int[] arr_2, int size_2, boolean choice){
        int[] arr_temp;
        int size_temp = 0;

        if(size_1 > size_2) {
            arr_temp = new int[size_1];
        } else {
            arr_temp = new int[size_2];
        }

        for (int i = 0; i < size_1; i++) {
            for (int j = 0; j < size_2; j++) {
                if(choice == true) {
                    if (arr_1[i] == arr_2[j]) {
                        arr_temp[size_temp++] = arr_1[i];
                        break;
                    }
                } else {
                    if (arr_1[i] != arr_2[j]) {
                        arr_temp[size_temp++] = arr_1[i];
                        break;
                    }
                }
            }
        }

        if(size_temp == 0) {
            return arr_temp;
        } else {
            for(int i = 0; i < size_temp; i++)
            {
                System.out.print(arr_temp[i] + " ");
            }
            return arr_temp;
        }
    }
    static void deleteNumbersFromArray(int[] arr, int size){
        int count = 0;

        for (int i = 0; i < size; i++)
        {
            for (int j = i; j < size; j++)
            {
                if (arr[i] == arr[j]) {
                    if (count == 2) {
                        for (int index = 0; index < size; index++)
                        {
                            if (arr[index] == arr[i]) {
                                for (int n = i; n < size; n++)
                                {
                                    arr[n] = arr[n + 1];
                                }
                                size--;
                            }
                        }
                        count = 0;
                    }  else  {
                        count++;
                    }
                }
            }
        }
    }
    static void deleteSeriesofElements(int[] arr, int size) {
        for (int i = 0; i < size; i++)
        {
            if (i == size - 1) {
                break;
            }
            if (arr[i] == arr[i + 1]) {
                for (int j = i + 1; j < size; j++)
                {
                    arr[j] = arr[j + 1];
                }
                size--;
            }
        }
    }
}
